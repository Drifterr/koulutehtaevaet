# Write your solution here
def tulosta_monesti(text, times):
    text = text + "\n"
    print(f"{text * times}")

# You can test your function by calling it within the following block
if __name__ == "__main__":
    tulosta_monesti("python", 3)