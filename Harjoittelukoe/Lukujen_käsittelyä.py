# Write your solution here
print("Syötä kokonaislukuja, 0 lopettaa:")
count = 0
sum = 0
positive = 0
negative = 0
while True:
    num = int(input("Number: "))
    if num == 0:
        break
    count += 1
    sum += num
    if num > 0:
        positive += 1
    else:
        negative += 1

print(f"Lukuja yhteensä {count}")
print(f"Lukujen summa {sum}")
print(f"Lukujen keskiarvo {sum/count}")
print(f"Positiivisia {positive}")
print(f"Negatiivisia {negative}")