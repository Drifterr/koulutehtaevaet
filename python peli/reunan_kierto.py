import pygame

pygame.init()
naytto = pygame.display.set_mode((640, 480))
naytto.fill((0, 0, 0))
robo = pygame.image.load("robo.png")

x = 0
y = 0
nopeus = 1
kello = pygame.time.Clock()

while True:
    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()

    naytto.blit(robo, (x, y))
    pygame.display.flip()

    if y + robo.get_height() < 480 and x == 0:
        y += nopeus
    if x + robo.get_width() < 640 and y + robo.get_height() == 479:
        x += nopeus
    if y > 0 and x + robo.get_width() == 639:
        y -= nopeus
    if x > 0 and y == 0:
        x -= nopeus

    kello.tick(1024)
