import pygame

pygame.init()
naytto = pygame.display.set_mode((640, 480))

robo = pygame.image.load("robo.png")

naytto.fill((0, 0, 0))

x = 1
y = 1
offset = 0
while y < 11:
    naytto.blit(robo, (x * 50 + offset, y * 30))
    x += 1
    if x == 11:
        y += 1
        x = 1
        offset += 10

pygame.display.flip()

while True:
    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()
