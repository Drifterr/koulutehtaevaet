import pygame

pygame.init()
naytto = pygame.display.set_mode((640, 480))

robo = pygame.image.load("robo.png")

naytto.fill((0, 0, 0))
leveys = robo.get_width()
korkeus = robo.get_height()

i = 1
while i < 11:
    naytto.blit(robo, (i * 50, 100))
    print(i)
    i += 1

pygame.display.flip()

while True:
    for tapahtuma in pygame.event.get():
        if tapahtuma.type == pygame.QUIT:
            exit()
