import csv
import mysql.connector
from mysql.connector import Error

db_config = {
    'host': 'localhost',
    'user': 'root',
    'password': '',
    'database': 'classicmodels'
}

def luo_taulu(cursor):
    create_table_query = """
    CREATE TABLE IF NOT EXISTS csv_data (
        id INT,
        aika VARCHAR(20),
        prosentti VARCHAR(10)
    )
    """
    cursor.execute(create_table_query)

def lisaa_tiedot_tietokantaan(cursor, csv_reader):
    insert_query = "INSERT INTO csv_data (id, aika, prosentti) VALUES (%s, %s, %s)"
    data = [(int(row[0]), row[1], row[2]) for row in csv_reader]
    cursor.executemany(insert_query, data)

def main():
    try:
        connection = mysql.connector.connect(**db_config)

        if connection.is_connected():
            print("Yhteys tietokantaan on avattu")
            cursor = connection.cursor()
            luo_taulu(cursor)
            
            with open('tiedosto.csv', 'r') as csv_file:
                csv_reader = csv.reader(csv_file, delimiter=';')
                next(csv_reader)
                lisaa_tiedot_tietokantaan(cursor, csv_reader)

            connection.commit()
            print("Tiedot on lisätty tietokantaan")

    except Error as e:
        print("Virhe:", e)
    finally:
        if 'cursor' in locals():
            cursor.close()
        if 'connection' in locals() and connection.is_connected():
            connection.close()
            print("Yhteys tietokantaan on suljettu")

if __name__ == "__main__":
    main()
