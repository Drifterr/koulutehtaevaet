function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min) + min);
}
let luku = getRandomInt(1, 10);
let arvaukset = 0;

function arvaus(num) {
  arvaukset++;
  if (num == luku) {
    alert("Oikein!");
    location.reload();
  } else if (arvaukset < 3) {
    if (num < luku) {
      alert(`Väärin. Luku on suurempi. Yrityksiä jäljellä: ${3 - arvaukset}`);
    } else if (num > luku) {
      alert(`Väärin. Luku on pienempi. Yrityksiä jäljellä: ${3 - arvaukset}`);
    }
  } else {
    alert(`Peli loppu. Oikea luku oli ${luku}`);
    location.reload();
  }
}
