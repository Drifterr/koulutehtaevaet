console.log("toimii") //testi, että ohjelma toimii
// määritellään muuttuja, vakio, pysyy aina samana
let canvas = document.getElementById('canvaasi'); //hae html documentista canvaasi-id:llä oleva elementti
//tehdään 2D
let ctx = canvas.getContext('2d');
// let myös määrittelee muuttujan, mutta sen arvoa voi muuttaa, var
let raf;
let inputStates = {} // näppäintenpainallukset tänne talteen
window.addEventListener('keydown', function(event) {
  console.log(event.key);
  if (event.key == "ArrowRight") {
    console.log('Oikea nuoli painettu');
    inputStates.right = true;
  }
  if (event.key == "ArrowLeft") {
    console.log('Vasen nuoli painettu');
    inputStates.left = true;
  }
}, false);

window.addEventListener('keyup', function(event) {
  console.log(event);
  if (event.key == "ArrowRight") {
    console.log('Oikea nuoli nostettu');
    inputStates.right = false;
  }
  else if (event.key == "ArrowLeft") {
    console.log('Vasen nuoli nostettu');
    inputStates.left = false;
  }
}, false);

// pallo, ominaisuuksia voi muuttaa
const ball = {
    x: 100,
    y: 100,
    vx: 7, //nopeus ja suunta
    vy: 2, 
    radius: 25, 
    color: 'red',
    draw() {
      ctx.beginPath();
      ctx.arc(this.x, this.y, this.radius, 0, Math.PI * 2, true);
      ctx.closePath();
      ctx.fillStyle = this.color;
      ctx.fill();
    }
}; // tähän loppuu pallon määrittely

const maila = {
  x: 260,
  y: 383,
  vx: 11, //nopeus ja suunta
  leveys: 90,
  korkeus: 15, 
  color: 'blue',
  draw() {
    ctx.fillStyle = this.color;
    ctx.fillRect(this.x, this.y, this.leveys, this.korkeus);
  }
}; // tähän loppuu mailan määrittely

// peliluuppi
// funktio on koodinpätkä, joka suoritetaan vain käskystä
function draw() {
    // tyhjennä kaikki
    ctx.clearRect(0,0, canvas.width, canvas.height);
    // piirrä pelimaailma
    ball.draw();
    maila.draw();

    // liikuta pelimaailmaa
    ball.x += ball.vx;
    ball.y += ball.vy;

    //liikuta mailaa oikealle
    if (inputStates.right) {
      maila.x = maila.x + maila.vx
    }

    //liikuta mailaa vasemmalle
    if (inputStates.left) {
      maila.x = maila.x - maila.vx;
    }
    
    // pallo törmää seinään
    if (ball.y + ball.radius > canvas.height || ball.y - ball.radius < 0) {
        ball.vy = -ball.vy; // pallo vaihtaa suuntaa
    }
    if (ball.x + ball.radius > canvas.width || ball.x - ball.radius < 0) {
        ball.vx = -ball.vx;
    }

    // törmääkö maila seinään
    if (maila.x < 0) {
      maila.x = 0;
    }
    if (maila.x + maila.leveys > canvas.width) {
      maila.x = canvas.width - maila.leveys;
    }
    
    // odota
    raf = window.requestAnimationFrame(draw);

//kissa koira ananas appelsiini
  var testX=ball.x;
  var testY=ball.y;

  if (testX < maila.x) testX=maila.x;
  else if (testX > (maila.x+maila.leveys)) testX=(maila.x+maila.leveys);
  if (testY < maila.y) testY=maila.y;
  else if (testY > (maila.y+maila.korkeus)) testY=(maila.y+maila.korkeus);

  var distX = ball.x - testX;
  var distY = ball.y - testY;
  var dista = Math.sqrt((distX*distX)+(distY*distY));
  if (dista <= ball.radius) {
    if (ball.x >= maila.x &&
      ball.x <= (maila.x+maila.leveys)) {
        ball.vy *= -1;
      }
      else if (ball.y >= maila.y &&
        ball.y <= (maila.y+maila.korkeus)) {
          ball.vx *= -1;
        }
  }
};

draw(); // suorita tämä funktio