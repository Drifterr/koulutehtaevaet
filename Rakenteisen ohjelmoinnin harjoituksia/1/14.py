# 14) Tee ohjelma, joka pyytää käyttäjältä kokonaislukuja ja laskee lukujen summan. Nolla lopettaa. 
print("Anna kokonaislukuja, niin lasken niiden summan (nolla lopettaa): ")
sum = 0
while True:
    a = int(input(""))
    sum += a
    if a == 0:
        break
print("Lukujen summa on", sum)