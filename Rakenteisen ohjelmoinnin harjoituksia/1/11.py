# 11) Tee ohjelma, joka kysyy paljonko sinulla on rahaa ja paljonko pitsa maksaa: 
a = float(input("Paljonko sinulla on rahaa? "))
b = float(input("Paljonko pitsa maksaa? "))
if b < a:
    print(f"Nauti pitsasta!\nRahaa jäi {a-b}")
else:
    print("Rahasi eivät riitä pitsaan.")