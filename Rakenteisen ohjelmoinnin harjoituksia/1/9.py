# 9) Tee ohjelma, joka pyytää käyttäjältä kokonaisluvun ja kertoo onko luku 60 tai sitä pienempi tai ei kumpaakaan: 
a = int(input("Anna luku: "))
if a == 60:
    print("Luku on tasan 60")
elif a < 60:
    print("Luku on pienempi kuin 60.")
else:
    print("Luku ei ole 60 eikä sitä pienempi.")