<?php
$ika = $_POST['ika'];
$email = $_POST['email']; 
$aihe = $_POST['aihe']; 
$paikka = $_POST['paikka']; 
$date = $_POST['date'];
$klo = $_POST['klo']; 

$txt = "$ika,$email,$aihe,$paikka,$date,$klo";
$myfile = file_put_contents('arvot.csv', $txt.PHP_EOL , FILE_APPEND | LOCK_EX);
?>

<!DOCTYPE html>
<html lang="fi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./css/styles.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/js/bootstrap.bundle.min.js"></script>
    <title>Ajanvaraustalletus</title>
</head>
<body class="dataa">
    <div class="container p-5 my-5 bg-dark text-white">
        <h1>Email: <?=$email ?></h1>
        <h1>Ikä: <?=$ika ?></h1>
        <h1>Aihe: <?=$aihe ?></h1>
        <h1>Paikka: <?=$paikka ?></h1>
        <h1>Päiväys: <?=$date ?></h1>
        <h1>Kellonaika: <?=$klo ?></h1>
        <br>
        <h2><a href="./varaukset.html">Näytä varaukset</a></h2>
    </div>
</body>
</html>