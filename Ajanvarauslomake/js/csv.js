window.onload = getText;
function getText() {
    let request = new XMLHttpRequest();
    request.open('GET', 'arvot.csv', true);
    request.send(null);
    request.onreadystatechange = function() {
        if (request.readyState === 4 && request.status === 200) {
            let type = request.getResponseHeader('Content-Type');
            if (type.indexOf("text") !== 1) {
                näytäVaraukset(request.responseText);
            }
        }
    }
}

function näytäVaraukset(teksti) {
    const taulukko = d3.csvParse(teksti);
    let html = "<table>"; 
        html+= "<thead>"; 
        html+= "<tr>"; 
        html+= "<th>Email</th>"; 
        html+= "<th>Ikä</th>"; 
        html+= "<th>Aihe</th>";
        html+= "<th>Paikka</th>";
        html+= "<th>Päivämäärä</th>";
        html+= "<th>Klo</th>";
        html+= "</tr>"; 
        html+= "</thead>"; 
        html+= "<tbody>"; 

    let i = 0; 
    while(i < taulukko.length) { 
        html+= "<tr>"; 
        html+= "<td>" +taulukko[i].Email+ "</td>"; 
        html+= "<td>" +taulukko[i].Ikä+ "</td>"; 
        html+= "<td>" +taulukko[i].Aihe+ "</td>"; 
        html+= "<td>" +taulukko[i].Paikka+ "</td>";
        html+= "<td>" +taulukko[i].Päivämäärä+ "</td>";
        html+= "<td>" +taulukko[i].Klo+ "</td>";
        html+= "</tr>"; i++; 
    } 
    html += "</tbody>"; 
    html += "</table>"; 
    document.getElementById("varaustaulukko").innerHTML = html; }