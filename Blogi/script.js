function luoteksti() {

    var paivamaara = document.getElementById("paivays");
    var otsikko = document.getElementById("otsikko");
    var teksti = document.getElementById("teksti");
    var tuotosDiv = document.getElementById("tuotos");
    var error = document.getElementById("error");

    if (paivamaara.value && otsikko.value && teksti.value != ""){
        var otsikkoElementti = document.createElement("h2");
        otsikkoElementti.innerHTML = otsikko.value + " " + paivamaara.value;
        tuotosDiv.appendChild(otsikkoElementti);

        var tekstiElementti = document.createElement("p");
        tekstiElementti.innerHTML = teksti.value;
        tuotosDiv.appendChild(tekstiElementti);
        error.innerHTML = "";
        
    }
    else {
        error.innerHTML = "Täytä kaikki kentät."
    }
    //var paivamaaraElementti = document.createElement("h2")
    //paivamaaraElementti.innerHTML = paivamaara.value;
    //tuotosDiv.appendChild(paivamaaraElementti);

    otsikko.value = "";
    paivays.value = "";
    teksti.value = "";
}  