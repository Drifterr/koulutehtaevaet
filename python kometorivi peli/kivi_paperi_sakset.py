import random
while True:
    mahdollisetValinnat = ["kivi", "paperi", "sakset"]
    kayttajanValinta = input("Valitse (kivi, paperi tai sakset): ")
    x = kayttajanValinta.lower()
    koneenValinta = random.choice(mahdollisetValinnat)
    print(f"Sinä valitsit {kayttajanValinta}, kone valitsi {koneenValinta}!\n")
    if x == koneenValinta:
        print("Tasapeli!")
    elif x == "kivi":
        if koneenValinta == "sakset":
            print("Kivi rikkoo sakset! Sinä voitit!")
        else:
            print("Paperi peittää kiven! Sinä hävisit!")
    elif x == "paperi":
        if koneenValinta == "kivi":
            print("Paperi peittää kiven! Sinä voitit!")
        else:
            print("Sakset leikkaa paperin! Sinä hävisit!")
    elif x == "sakset":
        if koneenValinta == "paperi":
            print("Sakset leikkaa paperin! Sinä voitit!")
        else:
            print("Kivi rikkoo sakset! Sinä hävisit!")

    pelaaUudestaan = input("\nPelaa uudestaan? (kyllä/ei): ")
    if pelaaUudestaan.lower() != "kyllä":
        break
